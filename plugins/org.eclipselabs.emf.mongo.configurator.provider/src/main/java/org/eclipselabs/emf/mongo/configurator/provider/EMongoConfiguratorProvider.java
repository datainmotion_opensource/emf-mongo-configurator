/**
 * Copyright (c) 2014 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.eclipselabs.emf.mongo.configurator.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.INodeChangeListener;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.IPreferenceChangeListener;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.NodeChangeEvent;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.PreferenceChangeEvent;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.impl.URIMappingRegistryImpl;
import org.eclipselabs.emf.mongo.converter.BigDecimalConverter;
import org.eclipselabs.emf.mongo.converter.BigIntegerConverter;
import org.eclipselabs.emf.mongo.converter.ConverterService;
import org.eclipselabs.mongo.osgi.api.MongoClientProvider;
import org.eclipselabs.mongo.osgi.api.MongoDatabaseProvider;
import org.eclipselabs.mongo.osgi.components.MongoClientProviderComponent;
import org.eclipselabs.mongo.osgi.components.MongoDatabaseProviderComponent;
import org.eclipselabs.mongo.osgi.configuration.ConfigurationProperties;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

import org.eclipselabs.emf.mongo.configurator.EMongoConfiguratorService;
import org.eclipselabs.emf.mongo.configurator.config.MongoDatabaseConfig;
import org.eclipselabs.emf.mongo.configurator.config.MongoInstanceConfig;
import org.eclipselabs.emf.mongo.configurator.constants.EMongoConfiguratorConstants;


/**
 * @author Jürgen Albert
 * @version 0.0.1
 */
public class EMongoConfiguratorProvider implements EMongoConfiguratorService{

  private ConverterService converterService;

  private static AtomicReference<Map<String, ServiceRegistration<MongoClientProvider>>> clientProviders = new AtomicReference<Map<String,ServiceRegistration<MongoClientProvider>>>(); 
  private static AtomicReference<Map<String, ServiceRegistration<MongoDatabaseProvider>>> databaseProviders = new AtomicReference<Map<String,ServiceRegistration<MongoDatabaseProvider>>>();
  private static ReentrantLock LOCK = null;
  private static AtomicReference<Preferences> configNode = new AtomicReference<Preferences>();

  /**
   * the activation method
   * @throws BackingStoreException in case the scope lookup throws an exception
   */
  public void activate(Map<String, Object> properties) throws BackingStoreException{
    //This should only be done once
    if(configNode.get() != null){
      return;
    }

    Assert.isTrue(properties.containsKey(SCOPE_PROPERTY), "Scope property is missing in the Service configuration");
    String scopeName = (String) properties.get(SCOPE_PROPERTY);
    IEclipsePreferences rootNode = getIPreferencesService().getRootNode();
    Assert.isTrue(rootNode.nodeExists(scopeName), "No scope found for " + scopeName);

    if(clientProviders.get() == null){
      clientProviders.set(new HashMap<String, ServiceRegistration<MongoClientProvider>>());
    }
    if(databaseProviders.get() == null){
      databaseProviders.set(new HashMap<String, ServiceRegistration<MongoDatabaseProvider>>());
    }
    if(LOCK == null){
      LOCK = new ReentrantLock();
    }
    registerConverters();

    handleConfigurationScope(rootNode.node(scopeName));
  }

  /**
   * looks for the {@link IPreferencesService}
   * @return the {@link IPreferencesService}
   */
  private IPreferencesService getIPreferencesService(){
    BundleContext bundleContext = FrameworkUtil.getBundle(getClass()).getBundleContext();
    ServiceReference<IPreferencesService> serviceReference = bundleContext.getServiceReference(IPreferencesService.class);
    return bundleContext.getService(serviceReference);
  }


  /**
   * Handles the given scopenode
   * @param scope the scope node to handle
   * @throws BackingStoreException
   */
  public void handleConfigurationScope(Preferences scope) throws BackingStoreException {
    configNode.set(scope.node(EMongoConfiguratorConstants.NODE_NAME));

    for(String child : configNode.get().childrenNames()){
      Preferences mongoNode = configNode.get().node(child);
      configureMongoInstanceWithNode(mongoNode);
      addPreferencesChangeListener(mongoNode);
    }

    if(configNode.get() instanceof IEclipsePreferences){

      ((IEclipsePreferences) configNode.get()).addNodeChangeListener(new INodeChangeListener() {

        @Override
        public void removed(NodeChangeEvent event) {
          unloadMongoConfig(event.getChild());
        }

        @Override
        public void added(NodeChangeEvent event) {
          addPreferencesChangeListener(event.getChild());
        }
      });
    }

  }

  /**
   * @param mongoNode unloads and unregisters a specific mongo instance config. Could also be a replica set congfig.
   */
  private void unloadMongoConfig(Preferences mongoNode){
    try{
      LOCK.lock();
      String instance = mongoNode.name();
      String instancePID = ConfigurationProperties.CLIENT_PID + "." + instance;
      ServiceRegistration<MongoClientProvider> serviceRegistration = clientProviders.get().remove(instancePID);

      Collection<ServiceRegistration<MongoDatabaseProvider>> dbs = getAndRemoveDataBaseProviderServiceRegistrationsForInstance(instance);

      for (ServiceRegistration<MongoDatabaseProvider> registration : dbs) {
        registration.unregister();
      }
      serviceRegistration.unregister();

    } finally {
      LOCK.unlock();
    }
  }

  /**
   * removes and returns all {@link MongoDatabaseProvider} {@link ServiceRegistration}s from the databaseProviders Map for the given instance 
   * @param instance the Instance we talk about
   * @return a {@link List} of {@link ServiceRegistration}s of {@link MongoDatabaseProvider} will never be null
   */
  private Collection<ServiceRegistration<MongoDatabaseProvider>> getAndRemoveDataBaseProviderServiceRegistrationsForInstance(
          String instance) {
    return getAndRemoveDataBaseProviderServiceRegistrationsForInstanceMap(instance).values();
  }

  /**
   * removes and returns all {@link MongoDatabaseProvider} {@link ServiceRegistration}s from the databaseProviders Map for the given instance 
   * @param instance the Instance we talk about
   * @return a {@link Map} of {@link ServiceRegistration}s of {@link MongoDatabaseProvider} will never be null
   */
  private Map<String, ServiceRegistration<MongoDatabaseProvider>> getAndRemoveDataBaseProviderServiceRegistrationsForInstanceMap(
          String instance) {
    Map<String, ServiceRegistration<MongoDatabaseProvider>> result = new HashMap<String, ServiceRegistration<MongoDatabaseProvider>>();
    for (String entry : databaseProviders.get().keySet()) {
      if(entry.startsWith(ConfigurationProperties.DATABASE_PID + "." + instance + ".")){
        result.put(entry, databaseProviders.get().remove(entry));
      }
    }
    return result;
  }


  /**
   * Reads the prefernces node for a specific mongodb instance to configure. It also registers or updates all the given  
   * @param mongoNode
   * @throws BackingStoreException 
   */
  private void configureMongoInstanceWithNode(Preferences mongoNode) throws BackingStoreException {
    if(configNode.get().nodeExists(mongoNode.absolutePath()) && !mongoNode.getBoolean(EMongoConfiguratorConstants.NODE_LOCKED, Boolean.FALSE)){
      try{
        LOCK.lock();
        BundleContext bundleContext = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
        String instance = mongoNode.name();
        String instancePID = ConfigurationProperties.CLIENT_PID + "." + instance;
        ServiceRegistration<MongoClientProvider> clientProviderServiceRegistration = clientProviders.get().get(instancePID);
        String baseUris = mongoNode.get(EMongoConfiguratorConstants.MONGO_BASEURIS, null);
        String databases = mongoNode.get(EMongoConfiguratorConstants.MONGO_DATABASES, null);
        // build credential string in format user1:password@db,user2:password@db2
        StringBuilder credentials = new StringBuilder(); 
        for (String database : databases.split(",")) {
        	if (credentials.length() > 0) {
        		credentials.append(",");
        	}
        	String user = mongoNode.get(database + "." + EMongoConfiguratorConstants.MONGO_USER_PROP, null);
        	String pwd = mongoNode.get(database + "." + EMongoConfiguratorConstants.MONGO_PASSWORD_PROP, "");
        	if (user != null) {
        		credentials.append(user + ":" + pwd + "@" + database);
        	}
        }
        Map<String, Object> mongoClientProperties  = createMapMongoClientOptions(mongoNode);
        baseUris = normalizeBaseUris(baseUris);

        URIMappingRegistryImpl.INSTANCE.put(URI.createURI("mongodb://" + instance + "/"), URI.createURI(baseUris.split("\\s*,\\s*")[0] + "/"));


        Map<String, Object> clientProps = new HashMap<String, Object>();

        clientProps.put(MongoClientProvider.PROP_URI, baseUris);
        clientProps.put(MongoClientProvider.PROP_CLIENT_ID, instance);
        clientProps.putAll(mongoClientProperties);
        clientProps.put(Constants.SERVICE_PID, instancePID);
        if(credentials.length() > 0){
            clientProps.put(MongoClientProvider.PROP_CREDENTIALS, credentials.toString());
        }

        MongoClientProviderComponent clientProviderComponent = null;

        if(clientProviderServiceRegistration == null){

          clientProviderComponent = new MongoClientProviderComponent();
          clientProviderComponent.activate(clientProps);
          clientProviderServiceRegistration = bundleContext.registerService(MongoClientProvider.class, clientProviderComponent, new Hashtable<String, Object>(clientProps));
          clientProviders.get().put(instancePID, clientProviderServiceRegistration);
        } else {
          clientProviderServiceRegistration.setProperties(new Hashtable<String, Object>(clientProps));
          clientProviderComponent = (MongoClientProviderComponent) bundleContext.getService(clientProviderServiceRegistration.getReference());
        }

        Map<String, ServiceRegistration<MongoDatabaseProvider>> toRemove = getSubMap(instance);

        if (databases != null) {
          for(String database : databases.split(",")){

            String databaseProviderPID = ConfigurationProperties.DATABASE_PID + "." + instance + "." + database;

            ServiceRegistration<MongoDatabaseProvider> dataBaseRegistration = toRemove.remove(databaseProviderPID); 

            Map<String, Object> databaseProps = new HashMap<String, Object>();

            databaseProps.put(MongoDatabaseProvider.PROP_ALIAS, database);
            databaseProps.put(MongoDatabaseProvider.PROP_DATABASE, database);
            databaseProps.put(Constants.SERVICE_PID, databaseProviderPID);


            if(dataBaseRegistration == null){
              MongoDatabaseProviderComponent mongoDatabaseProvider = new MongoDatabaseProviderComponent();
              mongoDatabaseProvider.bindMongoClientProvider(clientProviderComponent);
              mongoDatabaseProvider.activate(databaseProps);
              mongoDatabaseProvider.bindMongoClientProvider(clientProviderComponent);
              databaseProps.put("database.uri", mongoDatabaseProvider.getURI().toString());
              dataBaseRegistration = bundleContext.registerService(MongoDatabaseProvider.class, mongoDatabaseProvider, new Hashtable<String, Object>(databaseProps));
            } else {
              MongoDatabaseProvider service = bundleContext.getService(dataBaseRegistration.getReference());
              databaseProps.put("database.uri", service.getURI().toString());
              bundleContext.ungetService(dataBaseRegistration.getReference());
              dataBaseRegistration.setProperties(new Hashtable<String, Object>(databaseProps));
            }
            databaseProviders.get().put(databaseProviderPID, dataBaseRegistration);
          }
        }

        // no remove the Database Registrations that are not used anymore
        for (Entry<String, ServiceRegistration<MongoDatabaseProvider>> entry : toRemove.entrySet()) {
          ServiceRegistration<MongoDatabaseProvider> remove = databaseProviders.get().remove(entry.getKey());
          remove.unregister();
        }
      } finally {
        LOCK.unlock();
      }
    }
  }

  /**
   * Adds a {@link IPreferenceChangeListener} if the given node is of type {@link IEclipsePreferences}
   * @param mongoNode the Preference node to add the changelistener to
   */
  private void addPreferencesChangeListener(Preferences mongoNode) {
    if(mongoNode instanceof IEclipsePreferences){
      ((IEclipsePreferences) mongoNode).addPreferenceChangeListener(new IPreferenceChangeListener() {

        @Override
        public void preferenceChange(PreferenceChangeEvent event) {
          try {
            configureMongoInstanceWithNode(event.getNode());
          } catch (BackingStoreException e) {
            e.printStackTrace();
          }	
        }
      });
    }
  }

  /* (non-Javadoc)
   * @see de.dim.emongo.configurator.EMongoConfiguratorService#applyMongoInstanceConfig(de.dim.emongo.configurator.config.MongoInstanceConfig[])
   */
  @Override
  public void applyMongoInstanceConfig(MongoInstanceConfig... configs) throws BackingStoreException {
    if(configs == null || configs.length == 0 ){
      return;
    }
    for(MongoInstanceConfig config : configs){
      Preferences mongoInstanceNode = configNode.get().node(config.getInstanceName());
      mongoInstanceNode.putBoolean(EMongoConfiguratorConstants.NODE_LOCKED, Boolean.TRUE);
      mongoInstanceNode.put(EMongoConfiguratorConstants.MONGO_BASEURIS, config.getMongoBaseUri());
      createNodeMongoClientOptions(config.getClientProperties(), mongoInstanceNode);
      StringBuilder databases = new StringBuilder();
      for (MongoDatabaseConfig mongoDatabaseConfig : config.getDatabaseConfigs()) {
        if(databases.length() > 0){
          databases.append(",");
        }
        databases.append(mongoDatabaseConfig.getName());
        if(mongoDatabaseConfig.getUserName() != null) {
          mongoInstanceNode.put(mongoDatabaseConfig.getName() + "." + EMongoConfiguratorConstants.MONGO_USER_PROP, mongoDatabaseConfig.getUserName());
        } else {
          mongoInstanceNode.remove(mongoDatabaseConfig.getName() + "." + EMongoConfiguratorConstants.MONGO_USER_PROP);
        }
        if(mongoDatabaseConfig.getPassword()!= null) {
          mongoInstanceNode.put(mongoDatabaseConfig.getName() + "." + EMongoConfiguratorConstants.MONGO_PASSWORD_PROP, mongoDatabaseConfig.getPassword());
        } else {
          mongoInstanceNode.remove(mongoDatabaseConfig.getName() + "." + EMongoConfiguratorConstants.MONGO_PASSWORD_PROP);
        }
      }
      if(databases.length() > 0){
        mongoInstanceNode.put(EMongoConfiguratorConstants.MONGO_DATABASES, databases.toString());
      } else {
        mongoInstanceNode.remove(EMongoConfiguratorConstants.MONGO_DATABASES);
      }
      mongoInstanceNode.putBoolean(EMongoConfiguratorConstants.NODE_LOCKED, Boolean.FALSE);
    }
    configNode.get().flush();
  }

  /* (non-Javadoc)
   * @see de.dim.emongo.configurator.EMongoConfiguratorService#removeMongoInstanceConfig(java.lang.String)
   */
  @Override
  public void removeMongoInstanceConfig(String instanceName) throws BackingStoreException {
    if(configNode.get().nodeExists(instanceName)){
      Preferences node = configNode.get().node(instanceName);
      node.removeNode();
      configNode.get().flush();
    }
  }


  /**
   * Returns a {@link Map} containing only the databaseConfigs for the given instance
   * @param instance the instance to look for 
   * @return the desired {@link Map}
   */
  private Map<String, ServiceRegistration<MongoDatabaseProvider>> getSubMap(String instance) {
    String instancePid =  ConfigurationProperties.DATABASE_PID + "." + instance;
    Map<String, ServiceRegistration<MongoDatabaseProvider>> subMap = new HashMap<String, ServiceRegistration<MongoDatabaseProvider>>();
    for (Entry<String, ServiceRegistration<MongoDatabaseProvider>> entry : databaseProviders.get().entrySet()) {
      if(entry.getKey().startsWith(instancePid)){
        subMap.put(entry.getKey(), entry.getValue());
      }
    }
    return subMap;
  }


  public void deactivate(){
  }

  /**
   * Normalizes the given baseUris String. The baseUris can consist of more than one URI, separated by a ','. The method takes care, that every URI does not end with an '/'
   * @param baseUris the baseUris to normalize
   * @return the normalized baseUris
   */
  private String normalizeBaseUris(String baseUris) {
    StringBuilder response = new StringBuilder();
    for(String uri : baseUris.split("\\s*,\\s*")){
      response.append(",");
      response.append(uri.endsWith("/") ? uri.substring(0, uri.length() - 1) : uri );
    }
    return response.substring(1);
  }


  /**
   * Registers converters for non-native mongo-db values.
   */
  private void registerConverters() {
    converterService.addConverter(new BigIntegerConverter());
    converterService.addConverter(new BigDecimalConverter());
  }

  /**
   * @param service the {@link ConverterService} to set
   */
  void setConverterService(ConverterService service) {
    this.converterService = service;
  }

  /**
   * @param service the {@link ConverterService} to unset
   */
  void unsetConverterService(ConverterService service) {
    this.converterService = null;
  }

  @Override
  public List<MongoInstanceConfig> getAllMongoInstanceConfigurations() throws BackingStoreException {
    List<MongoInstanceConfig> list = new ArrayList<MongoInstanceConfig>(configNode.get().childrenNames().length);
    for(String child : configNode.get().childrenNames()){
      Preferences node = configNode.get().node(child);
      MongoInstanceConfig config = new MongoInstanceConfig();
      config.setInstanceName(child);
      config.setMongoBaseUri(node.get(EMongoConfiguratorConstants.MONGO_BASEURIS, null));
      String databases = node.get(EMongoConfiguratorConstants.MONGO_DATABASES, null);
      if (databases != null && databases.length() != 0) {
        config.getDatabaseConfigs().addAll(getDatabaseConfigurations(node));
      }
      list.add(config);
    }
    return list;
  }

  private Collection<? extends MongoDatabaseConfig> getDatabaseConfigurations(
          Preferences node) {
    String databasesString = node.get(EMongoConfiguratorConstants.MONGO_DATABASES, null);
    if (databasesString == null) {
      return null;
    }
    String[] split = databasesString.split(",");
    List<MongoDatabaseConfig> list = new ArrayList<MongoDatabaseConfig>(split.length);
    for (String databaseName : split) {
      MongoDatabaseConfig config = new MongoDatabaseConfig();
      config.setName(databaseName);
      config.setUserName(node.get(databaseName + "." + EMongoConfiguratorConstants.MONGO_USER_PROP, null));
      config.setPassword(node.get(databaseName + "." + EMongoConfiguratorConstants.MONGO_PASSWORD_PROP, null));
      list.add(config);
    }
    return list;
  }

  /**
   * Creates a properties map from OSGi {@link Preferences} for the mongo client
   * @param node the property source
   * @return the map with the properties from the preferences
   */
  private Map<String, Object> createMapMongoClientOptions(Preferences node)
  {
    Map<String, Object> resultMap = new HashMap<String, Object>();
    String description = node.get(MongoClientProvider.PROP_DESCRIPTION, null);

    if (description != null)
      resultMap.put(MongoClientProvider.PROP_DESCRIPTION, description);

    Integer connectionsPerHost =node.getInt(MongoClientProvider.PROP_CONNECTIONS_PER_HOST, -1);

    if (connectionsPerHost != -1)
      resultMap.put(MongoClientProvider.PROP_CONNECTIONS_PER_HOST, connectionsPerHost);

    Integer threadsAllowedToBlockForConnectionMultiplier = node.getInt(MongoClientProvider.PROP_THREADS_ALLOWED_TO_BLOCK_FOR_CONNECTION_MULTIPLIER, -1);

    if (threadsAllowedToBlockForConnectionMultiplier != -1)
      resultMap.put(MongoClientProvider.PROP_THREADS_ALLOWED_TO_BLOCK_FOR_CONNECTION_MULTIPLIER, threadsAllowedToBlockForConnectionMultiplier);

    Integer maxWaitTime = node.getInt(MongoClientProvider.PROP_MAX_WAIT_TIME, -1);

    if (maxWaitTime != -1)
      resultMap.put(MongoClientProvider.PROP_MAX_WAIT_TIME, maxWaitTime);

    Integer connectTimeout = node.getInt(MongoClientProvider.PROP_CONNECT_TIMEOUT, -1);

    if (connectTimeout != -1)
      resultMap.put(MongoClientProvider.PROP_CONNECT_TIMEOUT, connectTimeout);

    Integer socketTimeout = node.getInt(MongoClientProvider.PROP_SOCKET_TIMEOUT, -1);

    if (socketTimeout != -1)
      resultMap.put(MongoClientProvider.PROP_SOCKET_TIMEOUT, socketTimeout);

    Boolean socketKeepAlive = node.getBoolean(MongoClientProvider.PROP_SOCKET_KEEP_ALIVE, false);
    resultMap.put(MongoClientProvider.PROP_SOCKET_KEEP_ALIVE, socketKeepAlive);

    Boolean autoConnectRetry = node.getBoolean(MongoClientProvider.PROP_AUTO_CONNECT_RETRY, false);
    resultMap.put(MongoClientProvider.PROP_AUTO_CONNECT_RETRY, autoConnectRetry);

    Long maxAutoConnectRetryTime = node.getLong(MongoClientProvider.PROP_MAX_AUTO_CONNECT_RETRY_TIME, -1);

    if (maxAutoConnectRetryTime != -1)
      resultMap.put(MongoClientProvider.PROP_MAX_AUTO_CONNECT_RETRY_TIME, maxAutoConnectRetryTime);

    Boolean continueOnInsertError = node.getBoolean(MongoClientProvider.PROP_CONTINUE_ON_INSERT_ERROR, false);
    resultMap.put(MongoClientProvider.PROP_CONTINUE_ON_INSERT_ERROR, continueOnInsertError);

    Integer w = node.getInt(MongoClientProvider.PROP_W, 1);
    resultMap.put(MongoClientProvider.PROP_W, w);

    Integer wtimeout = node.getInt(MongoClientProvider.PROP_WTIMEOUT, 0);
    resultMap.put(MongoClientProvider.PROP_WTIMEOUT, wtimeout);

    Boolean j = node.getBoolean(MongoClientProvider.PROP_JOURNAL, false);
    resultMap.put(MongoClientProvider.PROP_JOURNAL, j);

    return resultMap;
  }

  /**
   * Creates OSGi {@link Preferences} from a given mongo client properties {@link Map}
   * @param map the source of the properties
   * @param node the target of the properties
   */
  private void createNodeMongoClientOptions(Map<String, Object> map, Preferences node) {
    String description = (String) map.get(MongoClientProvider.PROP_DESCRIPTION);

    if (description != null)
      node.put(MongoClientProvider.PROP_DESCRIPTION, description);

    Integer connectionsPerHost = getIntProperty(map.get(MongoClientProvider.PROP_CONNECTIONS_PER_HOST));

    if (connectionsPerHost != null)
      node.putInt(MongoClientProvider.PROP_CONNECTIONS_PER_HOST, connectionsPerHost);

    Integer threadsAllowedToBlockForConnectionMultiplier = getIntProperty(map.get(MongoClientProvider.PROP_THREADS_ALLOWED_TO_BLOCK_FOR_CONNECTION_MULTIPLIER));

    if (threadsAllowedToBlockForConnectionMultiplier != null)
      node.putInt(MongoClientProvider.PROP_THREADS_ALLOWED_TO_BLOCK_FOR_CONNECTION_MULTIPLIER, threadsAllowedToBlockForConnectionMultiplier);

    Integer maxWaitTime = getIntProperty(map.get(MongoClientProvider.PROP_MAX_WAIT_TIME));

    if (maxWaitTime != null)
      node.putInt(MongoClientProvider.PROP_MAX_WAIT_TIME, maxWaitTime);

    Integer connectTimeout = getIntProperty(map.get(MongoClientProvider.PROP_CONNECT_TIMEOUT));

    if (connectTimeout != null)
      node.putInt(MongoClientProvider.PROP_CONNECT_TIMEOUT, connectTimeout);

    Integer socketTimeout = getIntProperty(map.get(MongoClientProvider.PROP_SOCKET_TIMEOUT));

    if (socketTimeout != null)
      node.putInt(MongoClientProvider.PROP_SOCKET_TIMEOUT, socketTimeout);

    Boolean socketKeepAlive = getBoolProperty(map.get(MongoClientProvider.PROP_SOCKET_KEEP_ALIVE));

    if (socketKeepAlive != null)
      node.putBoolean(MongoClientProvider.PROP_SOCKET_KEEP_ALIVE, socketKeepAlive);

    Boolean autoConnectRetry = getBoolProperty(map.get(MongoClientProvider.PROP_AUTO_CONNECT_RETRY));

    if (autoConnectRetry != null)
      node.putBoolean(MongoClientProvider.PROP_AUTO_CONNECT_RETRY, autoConnectRetry);

    Long maxAutoConnectRetryTime = getLongProperty(map.get(MongoClientProvider.PROP_MAX_AUTO_CONNECT_RETRY_TIME));

    if (maxAutoConnectRetryTime != null)
      node.putLong(MongoClientProvider.PROP_MAX_AUTO_CONNECT_RETRY_TIME, maxAutoConnectRetryTime);

    Boolean continueOnInsertError = getBoolProperty(map.get(MongoClientProvider.PROP_CONTINUE_ON_INSERT_ERROR));

    if (continueOnInsertError != null)
      node.putBoolean(MongoClientProvider.PROP_CONTINUE_ON_INSERT_ERROR, continueOnInsertError);

    Integer w = getIntProperty(map.get(MongoClientProvider.PROP_W));

    if (w != null)
      node.putInt(MongoClientProvider.PROP_W, w);

    Integer wtimeout = getIntProperty(map.get(MongoClientProvider.PROP_WTIMEOUT));

    if (wtimeout != null)
      node.putInt(MongoClientProvider.PROP_WTIMEOUT, wtimeout);

    Boolean j = getBoolProperty(map.get(MongoClientProvider.PROP_JOURNAL));

    if (j != null)
      node.putBoolean(MongoClientProvider.PROP_JOURNAL, j);
  }

  /**
   * Creates an integer property from the given object value or <code>null</code>
   * @param value the object value to get an integer property
   * @return the integer value or <code>null</code>
   */
  private Integer getIntProperty(Object value) {
    if (value == null) {
      return null;
    }
    if (value instanceof Integer) {
      return (Integer)value;
    }
    try {
      Integer intValue = Integer.valueOf(value.toString());
      return intValue;
    } catch (Exception e) {
    }
    return null;
  }

  /**
   * Creates a long property from the given object value or <code>null</code>
   * @param value the object value to get a long property
   * @return the long value or <code>null</code>
   */
  private Long getLongProperty(Object value) {
    if (value == null) {
      return null;
    }
    if (value instanceof Long) {
      return (Long)value;
    }
    try {
      Long longValue = Long.valueOf(value.toString());
      return longValue;
    } catch (Exception e) {
    }
    return null;
  }

  /**
   * Creates a boolean property from the given object value or <code>null</code>
   * @param value the object value to get a boolean property
   * @return the boolean value or <code>null</code>
   */
  private Boolean getBoolProperty(Object value) {
    if (value == null) {
      return null;
    }
    if (value instanceof Boolean) {
      return (Boolean)value;
    }
    try {
      Boolean boolValue = Boolean.valueOf(value.toString());
      return boolValue;
    } catch (Exception e) {
    }
    return null;
  }
}
