/**
 * 
 */
package org.eclipselabs.emf.mongo.configurator.constants;

/**
 * @author Jürgen Albert
 *
 */
public interface EMongoConfiguratorConstants {

	public final static String NODE_NAME = "org.eclipselabs.emf.mongo.configurator";
	
	public static final String MONGO_INSTANCE_PROP = "mongo.instances";
	public static final String MONGO_BASEURIS = "baseUris";
	public static final String MONGO_DATABASES = "dataBases";
	public static final String MONGO_USER_PROP = "user";
	public static final String MONGO_PASSWORD_PROP = "password";

	public static final String TIMESTAMP = "timestamp";

	public static final String NODE_LOCKED = "editing_lock";
	
}
