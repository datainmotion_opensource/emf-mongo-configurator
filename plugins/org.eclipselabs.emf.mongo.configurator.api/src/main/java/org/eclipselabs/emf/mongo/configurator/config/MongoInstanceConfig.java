/**
 * Copyright (c) 2014 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.eclipselabs.emf.mongo.configurator.config;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This Class holds all Data necessary for a eMongo configuration 
 * @author Juergen Albert
 * @version 0.0.1
 */
public class MongoInstanceConfig extends ModelObject {

	String instanceName = null;
	String mongoBaseUri = null;
	Map<String, Object> clientProperties = new HashMap<String, Object>();
	List<MongoDatabaseConfig> databaseConfigs = new LinkedList<MongoDatabaseConfig>();

	/**
	 * @return the instanceName
	 */
	public String getInstanceName() {
		return instanceName;
	}

	/**
	 * @param instanceName the instanceName to set
	 */
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
		firePropertyChange("instanceName", this.instanceName, this.instanceName = instanceName);
	}

	/**
	 * @return the mongoBaseUri
	 */
	public String getMongoBaseUri() {
		return mongoBaseUri;
	}

	/**
	 * @param mongoBaseUri the mongoBaseUri to set
	 */
	public void setMongoBaseUri(String mongoBaseUri) {
		this.mongoBaseUri = mongoBaseUri;
		firePropertyChange("mongoBaseUri", this.mongoBaseUri, this.mongoBaseUri = mongoBaseUri);
	}

	/**
	 * @return the databaseConfigs
	 */
	public List<MongoDatabaseConfig> getDatabaseConfigs() {
		return databaseConfigs;
	}

	public Map<String, Object> getClientProperties() {
		return clientProperties;
	}

}
