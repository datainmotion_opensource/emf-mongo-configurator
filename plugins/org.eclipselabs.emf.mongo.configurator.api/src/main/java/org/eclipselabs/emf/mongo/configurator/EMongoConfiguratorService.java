/**
 * Copyright (c) 2014 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.eclipselabs.emf.mongo.configurator;

import java.util.List;

import org.eclipselabs.emf.mongo.configurator.config.MongoInstanceConfig;
import org.osgi.service.prefs.BackingStoreException;

/**
 * Interface defines the API to the {@link EMongoConfiguratorService}. 
 * Its purpose is to add new mongo configurations.
 * 
 * @author Jürgen Albert
 * @version 0.2.0
 */
public interface EMongoConfiguratorService {

	public static final String SCOPE_PROPERTY = "scope";
	
	/**
	 * Applies the given {@link MongoInstanceConfig} Objects to the System
	 * @param configs an Array of {@link MongoInstanceConfig}s
	 * @throws BackingStoreException thrown in case no connection to the zookeeper is available
	 */
	public void applyMongoInstanceConfig(MongoInstanceConfig... configs) throws BackingStoreException;
	
	/**
	 * @param instanceName
	 * @throws BackingStoreException
	 */
	public void removeMongoInstanceConfig(String instanceName) throws BackingStoreException;
	
	/**
	 * @return
	 * @throws BackingStoreException
	 */
	public List<MongoInstanceConfig> getAllMongoInstanceConfigurations() throws BackingStoreException;
}
