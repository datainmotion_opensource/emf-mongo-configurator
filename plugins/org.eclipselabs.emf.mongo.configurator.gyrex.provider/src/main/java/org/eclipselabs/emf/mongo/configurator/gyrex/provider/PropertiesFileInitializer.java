package org.eclipselabs.emf.mongo.configurator.gyrex.provider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.util.Properties;

import org.eclipse.core.runtime.URIUtil;
import org.eclipselabs.emf.mongo.configurator.EMongoConfiguratorService;
import org.eclipselabs.emf.mongo.configurator.config.MongoDatabaseConfig;
import org.eclipselabs.emf.mongo.configurator.config.MongoInstanceConfig;
import org.osgi.service.prefs.BackingStoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Inits available mongo connections from property file found in the servers etc directory or in the directory provided in by the 
 * dim.server.etc system property
 * @author Juergen Albert
 */
public class PropertiesFileInitializer {
	
	public static final String DIM_ETC = "dim.server.etc";
	
	private static final Logger LOG = LoggerFactory.getLogger(PropertiesFileInitializer.class);
	
	private static EMongoConfiguratorService eMongoConfiguratorService;
	
	public void activate(){
		String confDirString = System.getProperty(DIM_ETC);
		if(confDirString == null){
			confDirString = System.getProperty("eclipse.home.location")  + "etc";
		}

		if(confDirString == null || confDirString.isEmpty()){
			return;
		}
		
		if(!confDirString.endsWith("/") && !confDirString.endsWith("/"))
			confDirString += "/";
		final File confDir;
		if(confDirString.startsWith("file:")){
			java.net.URI fileUri = java.net.URI.create(confDirString);
			confDir = URIUtil.toFile(fileUri);
		} else{
			confDir = new File(confDirString);
		}


		// grep all files with the file type .properties
		File[] propertyFiles = confDir.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return dir.equals(confDir) && name.endsWith(".properties");
			}
		});

		//for the case that a propertiesFile has been deleted
		if(propertyFiles != null){
			LOG.trace("found {} property Files to Load", propertyFiles.length);
			for(File file : propertyFiles){
				String absolutePath = file.getAbsolutePath();
				LOG.trace("Loading properties file [ {} ]", absolutePath);
				loadPropertiesFromFile(file);
			}
		}
	}

	/**
	 * @param file the Properties File to load
	 */
	private void loadPropertiesFromFile(File file) {
		Properties props = new Properties();
		InputStream in;
		try {
			in = new FileInputStream(file);
			props.load(in);
			String instances = props.getProperty("mongo.instances");
			if(instances != null && !instances.isEmpty()){
				registerMongoInstances(instances.split(","), props);
			} else {
				LOG.warn("no mongo.instance property found in file {}", file.getAbsolutePath());
			}
		} catch (Exception e) {
			LOG.error("Could not load proprties File: " + file.getAbsolutePath(), e);
		}
	}
	
	/**
	 * Registers all the given instances, by gathering the necessary informations from the server conf file.
	 *
	 * This is a sample configuration: <br>
	 * <code>
	 * mongo.instances=test1,test2
	 * test1.baseUris=mongodb://localhost,mongodb://someotherhost:1234
	 * test1.databases=bal,test
	 * test1.bla.user=john
	 * test1.bla.password=test
	 * 
	 * test2.baseUris=mondodb://123.123.123.123
	 * test2.databases=somethingelse
	 * </code>
	 *
	 * @param instances
	 * 			The MonogDB instance names used in the server conf file
	 */
	private void registerMongoInstances(String[] instances, Properties props) {
		
		for (int i = 0; i < instances.length; i++) {
			String instance = instances[i];
			
			MongoInstanceConfig config = new MongoInstanceConfig();
			config.setInstanceName(instance);
			
			String baseUris = props.getProperty(instance + ".baseUris");
			String databases = props.getProperty(instance + ".dataBases");
			config.setMongoBaseUri(baseUris);
			for(String database : databases.split(",")){
				MongoDatabaseConfig dbConfig = new MongoDatabaseConfig();
				dbConfig.setName(database);
				String user = props.getProperty(instance + "." + database + ".user");
				String pwd = props.getProperty(instance + "." + database + ".password");
				if(user != null){
					dbConfig.setUserName(user);
				}
				if(pwd != null){
					dbConfig.setPassword(pwd);
				}
				config.getDatabaseConfigs().add(dbConfig);
			}
			try {
				eMongoConfiguratorService.applyMongoInstanceConfig(config);
			} catch (BackingStoreException e) {
				LOG.error("Could not apply mongoInstanceconfig " + config.getInstanceName(), e);
			}
		}
		
	}

	/**
	 * Sets the OSGi service instance of type @link{EMongoConfiguratorService} 
	 * using OSGi DS
	 * @param eMongoConfiguratorService the OSGi service instance to set
	 */
	public static void setEMongoConfiguratorService(EMongoConfiguratorService eMongoConfiguratorService) {
		PropertiesFileInitializer.eMongoConfiguratorService = eMongoConfiguratorService;
	}

	/**
	 * Unsets the OSGi service instance of type @link{EMongoConfiguratorService} 
	 * using OSGi DS
	 * @param eMongoConfiguratorService the OSGi service instance to un-set
	 */                     
	public static void unsetEMongoConfiguratorService(EMongoConfiguratorService eMongoConfiguratorService) {
		PropertiesFileInitializer.eMongoConfiguratorService = null;
	}

	/**
	 * Returns the OSGi service instance of type @link{EMongoConfiguratorService}
	 * @return the OSGi service instance of type @link{EMongoConfiguratorService}
	 */
	public static EMongoConfiguratorService getEMongoConfiguratorService() {
		return PropertiesFileInitializer.eMongoConfiguratorService;
	}

	
}
