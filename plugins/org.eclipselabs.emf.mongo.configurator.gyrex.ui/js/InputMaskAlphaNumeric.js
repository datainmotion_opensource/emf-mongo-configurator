function handleEvent(event) {
    var regexp = new RegExp(/^[a-zA-Z0-9:]*$/);
    if (!regexp.test(event.text)) {
        event.doit = false;
    }
}