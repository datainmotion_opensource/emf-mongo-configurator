package org.eclipselabs.emf.mongo.configurator.gyrex.ui;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.gyrex.admin.ui.internal.widgets.NonBlockingStatusDialog;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import org.eclipselabs.emf.mongo.configurator.config.MongoDatabaseConfig;
import org.eclipselabs.emf.mongo.configurator.config.MongoInstanceConfig;

/**
 * Dialog to create or update a mongo database configuration. 
 *
 * @author sdoerl
 * @since 11.03.2015
 */
@SuppressWarnings("restriction")
public class MongoDataBaseConfigEditDialog extends NonBlockingStatusDialog {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private static final String CREATE_HEADING = "Add a new database configuration";
	private static final String EDIT_HEADING = "Modify a database configuration";
	private MongoDatabaseConfig config;
	private MongoDatabaseConfig configBackup;
	private boolean isCreation;
	private MongoInstanceConfig instanceConfig;
	
	/**
	 * Edit-Constructor
	 * @param parent the parent {@link Shell}
	 * @param config the {@link MongoDatabaseConfig}
	 */
	public MongoDataBaseConfigEditDialog(Shell parent, MongoDatabaseConfig config) {
		super(parent);
		this.configBackup = copyConfig(config);
		this.config = config;
		this.isCreation = false;
	}
	
	/**
	 * Create-Constructor
	 * @param parent the parent {@link Shell}
	 * @param instanceConfig the {@link MongoInstanceConfig}
	 */
	public MongoDataBaseConfigEditDialog(Shell parent, MongoInstanceConfig instanceConfig) {
		super(parent);
		this.config = new MongoDatabaseConfig();
		this.isCreation = true;
		this.instanceConfig = instanceConfig;
	}

	private MongoDatabaseConfig copyConfig(MongoDatabaseConfig toCopy) {
		MongoDatabaseConfig copy = new MongoDatabaseConfig();
		copy.setName(toCopy.getName());
		copy.setUserName(toCopy.getUserName());
		copy.setPassword(toCopy.getPassword());
		return copy;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		final Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(new GridLayout(2, false));
		setTitle(isCreation ? CREATE_HEADING : EDIT_HEADING);
		
		GridData gd = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd.widthHint = 120;
		GridDataFactory gdf = GridDataFactory.createFrom(gd);
		
		Label lblName = new Label(composite, SWT.NONE);
		lblName.setText("Name");
		gdf.applyTo(lblName);
		
		Text txtName = new Text(composite, SWT.BORDER);
		txtName.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		Label lblUserName = new Label(composite, SWT.NONE);
		lblUserName.setText("Username");
		gdf.applyTo(lblUserName);
		Text txtUserName = new Text(composite, SWT.BORDER);
		txtUserName.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		Label lblPassword = new Label(composite, SWT.NONE);
		lblPassword.setText("Password");
		gdf.applyTo(lblPassword);
		Text txtPassword = new Text(composite, SWT.BORDER);
		txtPassword.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		Realm realm = SWTObservables.getRealm(Display.getCurrent());
		DataBindingContext dbc = new DataBindingContext(realm);
		
		IObservableValue txtNameOV = SWTObservables.observeText(txtName, SWT.Modify);
		IObservableValue modelNameOV = BeanProperties.value("name").observe(realm, config);
		dbc.bindValue(txtNameOV, modelNameOV);
		
		IObservableValue txtUserNameOV = SWTObservables.observeText(txtUserName, SWT.Modify);
		IObservableValue modelUserNameOV = BeanProperties.value("userName").observe(realm, config);
		dbc.bindValue(txtUserNameOV, modelUserNameOV);
		
		IObservableValue txtPasswordOV = SWTObservables.observeText(txtPassword, SWT.Modify);
		IObservableValue modelPasswordOV = BeanProperties.value("password").observe(realm, config);
		dbc.bindValue(txtPasswordOV, modelPasswordOV);
		return composite;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#cancelPressed()
	 */
	@Override
	protected void cancelPressed() {
		if (!isCreation) {
			revertModelChanges();
		}
		super.cancelPressed();
	}
	
	@Override
	protected void okPressed() {
		if (isCreation) {
			this.instanceConfig.getDatabaseConfigs().add(this.config);
		}
		super.okPressed();
	}

	private void revertModelChanges() {
		this.config.setName(configBackup.getName());
		this.config.setUserName(configBackup.getUserName());
		this.config.setPassword(configBackup.getPassword());
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#isResizable()
	 */
	protected boolean isResizable() {
		return true;
	};
}
