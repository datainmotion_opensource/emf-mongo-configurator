package org.eclipselabs.emf.mongo.configurator.gyrex.ui;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.conversion.IConverter;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.gyrex.admin.ui.internal.widgets.NonBlockingStatusDialog;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.rap.rwt.RWT;
import org.eclipse.rap.rwt.scripting.ClientListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.osgi.framework.FrameworkUtil;

import org.eclipselabs.emf.mongo.configurator.config.MongoInstanceConfig;

/**
 * Dialog to create or edit a mongo instance configuration.
 *
 * @author sdoerl
 * @since 11.03.2015
 */
@SuppressWarnings("restriction")
public class MongoInstanceConfigEditDialog extends NonBlockingStatusDialog{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private static final String CREATE_HEADING = "Create a new instance configuration";
	private static final String EDIT_HEADING = "Modify an instance configuration";
	private MongoInstanceConfig instanceConfig;
	private MongoInstanceConfig copyInstanceConfig;
	private boolean isCreate;
	private Text txtBaseURI;
	private String script;
	private ClientListener maskAlphaNumeric;

	/**
	 * Constructor
	 * @param parent the parent {@link Shell}
	 * @param instanceConfig the {@link MongoInstanceConfig} to edit
	 * @param isCreate <code>true</code> to create a new config, <code>false</code> to edit an existing one
	 */
	public MongoInstanceConfigEditDialog(Shell parent, MongoInstanceConfig instanceConfig, boolean isCreate) {
		super(parent);
		this.isCreate = isCreate;
		this.copyInstanceConfig = copyInstanceConfig(instanceConfig);
		this.instanceConfig = instanceConfig;
	}
	
	private MongoInstanceConfig copyInstanceConfig(MongoInstanceConfig source) {
		MongoInstanceConfig target = new MongoInstanceConfig();
		target.setInstanceName(source.getInstanceName());
		target.setMongoBaseUri(source.getMongoBaseUri());
		target.getDatabaseConfigs().addAll(source.getDatabaseConfigs());
		return target;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		final Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(new GridLayout(3, false));
		setTitle(isCreate ? CREATE_HEADING : EDIT_HEADING);
		
		Label lblInstanceName = new Label(composite, SWT.NONE);
		lblInstanceName.setText("Instance name");
		lblInstanceName.setVisible(isCreate);
		Text txtInstanceName = new Text(composite, SWT.BORDER);
		txtInstanceName.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1));
		txtInstanceName.setVisible(isCreate);
		
		Label lblBaseUri = new Label(composite, SWT.NONE);
		lblBaseUri.setText("Mongo Base URI");
		lblBaseUri.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		((GridData)lblBaseUri.getLayoutData()).widthHint = 150;
		
		Label lblScheme = new Label(composite, SWT.NONE);
		lblScheme.setData(RWT.MARKUP_ENABLED, Boolean.TRUE);
		Color gray = Display.getCurrent().getSystemColor(SWT.COLOR_DARK_GRAY);
		lblScheme.setForeground(gray);
		lblScheme.setText("mongodb://");
		
		txtBaseURI = new Text(composite, SWT.BORDER);
		txtBaseURI.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		((GridData)txtBaseURI.getLayoutData()).widthHint = 300;
		
		script = getFileContent("js/InputMaskAlphaNumeric.js");
		maskAlphaNumeric = new ClientListener(script);
		txtBaseURI.addListener(SWT.Verify, maskAlphaNumeric);
		Realm realm = SWTObservables.getRealm(Display.getCurrent());
		DataBindingContext dbc = new DataBindingContext(realm);
		if (isCreate) {
			IObservableValue txtInstanceNameOV = SWTObservables.observeText(txtInstanceName, SWT.Modify);
			IObservableValue modelInstanceNameOV = BeanProperties.value("instanceName").observe(realm, instanceConfig);
			dbc.bindValue(txtInstanceNameOV, modelInstanceNameOV);
		}
		
		IObservableValue txtBaseUriOV = SWTObservables.observeText(txtBaseURI, SWT.Modify);
		
		IObservableValue modelBaseUriOV = BeanProperties.value("mongoBaseUri").observe(realm, instanceConfig);
		UpdateValueStrategy uvsModelToTarget = new UpdateValueStrategy();
		uvsModelToTarget.setConverter(new MongoBaseUriConverter(true));
		UpdateValueStrategy uvsTargetToModel = new UpdateValueStrategy();
		uvsTargetToModel.setConverter(new MongoBaseUriConverter(false));
		
		dbc.bindValue(txtBaseUriOV, modelBaseUriOV, uvsTargetToModel, uvsModelToTarget);
		return composite;
	}
	
	private String getFileContent(String filePath) {
		URL entry = FrameworkUtil.getBundle(getClass()).getEntry(filePath);
        Scanner scanner = null;
		try {
			try {
				scanner = new Scanner(entry.openStream(), "UTF-8");
			} catch (IOException e) {
				e.printStackTrace();
			}
			String content = scanner.useDelimiter("\\A").next();
			return content;
		} finally {
			if(scanner != null){
				scanner.close();
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#isResizable()
	 */
	@Override
	protected boolean isResizable() {
		return true;
	}
	
	@Override
	protected void okPressed() {
		super.okPressed();
	}
	
	@Override
	protected void cancelPressed() {
		if (!isCreate) {
			revertChanges();
		}
		super.cancelPressed();
	}

	private void revertChanges() {
		this.instanceConfig.setInstanceName(this.copyInstanceConfig.getInstanceName());
		this.instanceConfig.setMongoBaseUri(this.copyInstanceConfig.getMongoBaseUri());
		this.instanceConfig.getDatabaseConfigs().clear();
		this.instanceConfig.getDatabaseConfigs().addAll(this.copyInstanceConfig.getDatabaseConfigs());
	}

	@Override
	public boolean close() {
		if (txtBaseURI != null && !txtBaseURI.isDisposed()) {
			txtBaseURI.removeListener(SWT.Verify, maskAlphaNumeric);
		}
		return super.close();
	}
	
	class MongoBaseUriConverter implements IConverter {
		
		private boolean modelToTarget;
		private final String mongoScheme = "mongodb://";
		public MongoBaseUriConverter(boolean modelToTarget) {
			this.modelToTarget = modelToTarget;
		}

		@Override
		public Object convert(Object fromObject) {
			if (fromObject != null && ((String)fromObject).length() !=0) {
				if (modelToTarget) {
					return ((String)fromObject).replace(mongoScheme, "");
				} else {
					return mongoScheme + (String)fromObject;
				}
			}
			return fromObject;
		}

		@Override
		public Object getFromType() {
			return String.class;
		}

		@Override
		public Object getToType() {
			return String.class;
		}
		
	}
}
