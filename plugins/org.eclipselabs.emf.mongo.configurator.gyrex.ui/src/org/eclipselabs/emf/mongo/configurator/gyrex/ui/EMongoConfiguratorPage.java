/**
 * Copyright (c) 2015 Data In Motion UG and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Sebastian Doerl - initial API and implementation
 */
package org.eclipselabs.emf.mongo.configurator.gyrex.ui;


import java.util.Collection;
import java.util.List;

import org.eclipse.gyrex.admin.ui.internal.widgets.Infobox;
import org.eclipse.gyrex.rap.application.Page;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.rap.rwt.RWT;
import org.eclipse.rap.rwt.widgets.DialogCallback;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.osgi.service.prefs.BackingStoreException;

import org.eclipselabs.emf.mongo.configurator.EMongoConfiguratorService;
import org.eclipselabs.emf.mongo.configurator.config.MongoDatabaseConfig;
import org.eclipselabs.emf.mongo.configurator.config.MongoInstanceConfig;


/**
 * AdminPage to create, update or delete mongo instance configurations and
 * mongo database configurations.
 *
 * @author sdoerl
 * @since 09.03.2015
 */
@SuppressWarnings("restriction")
public class EMongoConfiguratorPage extends Page {

	private static EMongoConfiguratorService eMongoConfiguratorService;
	private Composite pageComposite;
	private ISelectionChangedListener selectionChangedListener;
	private TreeViewer tv;
	private Button btnAddMongoInstanceConfig;
	private Button btnAddMongoDatabaseConfig;
	private Button btnEdit;
	private Button btnDelete;
	private Composite buttonComposite;
	private BtnListener btnListener;	/**
	 * Creates a new instance.
	 */
	public EMongoConfiguratorPage() {
		setTitle("EMongo Configurator");
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.gyrex.admin.ui.pages.AdminPage#createControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public Control createControl(final Composite parent) {
		pageComposite = parent;
		Composite container = new Composite(pageComposite, SWT.NONE);
		container.setLayout(new GridLayout(2, false));
		
		Infobox infobox = new Infobox(container);
		infobox.addHeading("Configuring EMongo Instances");
		infobox.addParagraph("EMongo instances are identified via their mongo base URI. "
				+ "Each EMongo can have several different database configurations.<br/> "
				+ "Database configurations hold the database name and the username "
				+ "and password to access the specified database.");
		infobox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		
		Composite tableComposite = new Composite(container, SWT.NONE);
		tableComposite.setLayout(new GridLayout());
		tableComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		tv = new TreeViewer(tableComposite, SWT.SINGLE | SWT.FULL_SELECTION | SWT.V_SCROLL | SWT.BORDER | SWT.SINGLE | SWT.VIRTUAL);
		tv.getTree().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		tv.getTree().setData(RWT.MARKUP_ENABLED, Boolean.TRUE);
		tv.setContentProvider(new TreeViewContentProvider());
		reloadInput();
		tv.setLabelProvider(new LabelProvider() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public String getText(Object element) {
				if (element instanceof MongoInstanceConfig) {
					return "<strong>" + ((MongoInstanceConfig)element).getInstanceName() + "</strong>";
				} else if (element instanceof MongoDatabaseConfig) {
					return ((MongoDatabaseConfig)element).getName();
				}
				return super.getText(element);
			}
		});
		selectionChangedListener = new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				btnAddMongoDatabaseConfig.setEnabled(!event.getSelection().isEmpty());
				btnEdit.setEnabled(!event.getSelection().isEmpty());
				btnDelete.setEnabled(!event.getSelection().isEmpty());
			}
		};
		tv.addSelectionChangedListener(selectionChangedListener);

		buttonComposite = new Composite(container, SWT.NONE);
		buttonComposite.setLayout(new GridLayout());
		buttonComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
		btnListener = new BtnListener();
		btnAddMongoInstanceConfig = new Button(buttonComposite, SWT.PUSH);
		btnAddMongoInstanceConfig.setText("Add Instance Config");
		btnAddMongoInstanceConfig.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		btnAddMongoDatabaseConfig = new Button(buttonComposite, SWT.PUSH);
		btnAddMongoDatabaseConfig.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		btnAddMongoDatabaseConfig.setText("Add Database Config");
		btnAddMongoDatabaseConfig.setEnabled(tv.getSelection() != null && !tv.getSelection().isEmpty());
		btnEdit = new Button(buttonComposite, SWT.PUSH);
		btnEdit.setText("Edit");
		btnEdit.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		btnEdit.setEnabled(tv.getSelection() != null && !tv.getSelection().isEmpty());
		btnDelete = new Button(buttonComposite, SWT.PUSH);
		btnDelete.setText("Delete");
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		btnDelete.setEnabled(tv.getSelection() != null && !tv.getSelection().isEmpty());
		for (Control e : buttonComposite.getChildren()) {
			if (e instanceof Button) {
				((Button)e).addSelectionListener(btnListener);
			}
		}
			
		return pageComposite;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.gyrex.admin.ui.pages.AdminPage#deactivate()
	 */
	@Override
	public void deactivate() {
		if (tv != null && !tv.getTree().isDisposed() && selectionChangedListener != null) {
			tv.removeSelectionChangedListener(selectionChangedListener);
		}
		if (buttonComposite != null && !buttonComposite.isDisposed()) {
			for (Control c : buttonComposite.getChildren()) {
				if (c instanceof Button && btnListener != null) {
					((Button)c).removeSelectionListener(btnListener);
				}
			}
		}
		super.deactivate();
	}
	
	public static void bindEMongoConfiguratorService(EMongoConfiguratorService service) {
		EMongoConfiguratorPage.eMongoConfiguratorService = service;
	}
	
	public static void releaseEMongoConfiguratorService(EMongoConfiguratorService service) {
		EMongoConfiguratorPage.eMongoConfiguratorService = null;
	}
	
	class BtnListener extends SelectionAdapter {
		/**
		 * serialVersionUID
		 */
		private static final long serialVersionUID = 1L;

		/* (non-Javadoc)
		 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
		 */
		@Override
		public void widgetSelected(SelectionEvent e) {
			if (e.getSource().equals(btnAddMongoInstanceConfig)) {
				createMongoInstanceDialog(new MongoInstanceConfig(), true);
			} else if (e.getSource().equals(btnAddMongoDatabaseConfig)) {
				createMongoDatabaseConfigDialog(null);
			} else if (e.getSource().equals(btnEdit)) {
				final Object element = ((IStructuredSelection)tv.getSelection()).getFirstElement();
				if (element instanceof MongoInstanceConfig) {
					createMongoInstanceDialog((MongoInstanceConfig) element, false);
				} else if (element instanceof MongoDatabaseConfig) {
					createMongoDatabaseConfigDialog((MongoDatabaseConfig) element);
				}
			} else if (e.getSource().equals(btnDelete)) {
				Object element = ((IStructuredSelection)tv.getSelection()).getFirstElement();
				if (element instanceof MongoInstanceConfig) {
					try {
						eMongoConfiguratorService.removeMongoInstanceConfig(((MongoInstanceConfig) element).getInstanceName());
						reloadInput();
					} catch (BackingStoreException e1) {
						e1.printStackTrace();
					}
				} else if (element instanceof MongoDatabaseConfig) {
					MongoInstanceConfig parent = (MongoInstanceConfig) tv.getTree().getSelection()[0].getParentItem().getData();
					parent.getDatabaseConfigs().remove(element);
					try {
						eMongoConfiguratorService.applyMongoInstanceConfig(parent);
						reloadInput();
					} catch (BackingStoreException e1) {
						e1.printStackTrace();
					}
				}
			} else {
				super.widgetSelected(e);
			}
		}
	}
	
	private void createMongoDatabaseConfigDialog(final MongoDatabaseConfig config) {
		final MongoDataBaseConfigEditDialog dialog;
		if (config != null) {
			dialog = new MongoDataBaseConfigEditDialog(Display.getCurrent().getActiveShell(), config);
		} else {
			Object element = ((IStructuredSelection)tv.getSelection()).getFirstElement();
			if (element != null && element instanceof MongoInstanceConfig) {
				dialog = new MongoDataBaseConfigEditDialog(Display.getCurrent().getActiveShell(), (MongoInstanceConfig) element);
			} else if (element != null && element instanceof MongoDatabaseConfig) {
				dialog = new MongoDataBaseConfigEditDialog(Display.getCurrent().getActiveShell(), (MongoInstanceConfig) tv.getTree().getSelection()[0].getParentItem().getData());
			} else {
				return;
			}
		}
		dialog.openNonBlocking(new DialogCallback() {
			
			/**
			 * serialVersionUID
			 */
			private static final long serialVersionUID = 1L;

			/* (non-Javadoc)
			 * @see org.eclipse.rap.rwt.widgets.DialogCallback#dialogClosed(int)
			 */
			@Override
			public void dialogClosed(int returnCode) {
				if (returnCode == Window.OK) {
					try {
						Object element = ((IStructuredSelection)tv.getSelection()).getFirstElement();
						if (element instanceof MongoDatabaseConfig) {
							eMongoConfiguratorService.applyMongoInstanceConfig((MongoInstanceConfig) tv.getTree().getSelection()[0].getParentItem().getData());
						} else if (element instanceof MongoInstanceConfig) {
							eMongoConfiguratorService.applyMongoInstanceConfig((MongoInstanceConfig) element);
						}
						reloadInput();
					} catch (BackingStoreException e) {
						e.printStackTrace();
					}
				}
				//do nothing
			}
		});
	}
	
	private void createMongoInstanceDialog(final MongoInstanceConfig config, final boolean isCreation){
		final MongoInstanceConfigEditDialog dialog = new MongoInstanceConfigEditDialog(Display.getCurrent().getActiveShell(), config, isCreation);
		dialog.openNonBlocking(new DialogCallback() {

			/** serialVersionUID */
			private static final long serialVersionUID = 1L;

			/* (non-Javadoc)
			 * @see org.eclipse.rap.rwt.widgets.DialogCallback#dialogClosed(int)
			 */
			@Override
			public void dialogClosed(int returnCode) {
				if (returnCode == Window.OK) {
					try {
						eMongoConfiguratorService.applyMongoInstanceConfig(config);
						reloadInput();
					} catch (BackingStoreException e) {
						e.printStackTrace();
					}
				} 
			}
		});
	}

	protected void reloadInput() {
		try {
			tv.setInput(eMongoConfiguratorService.getAllMongoInstanceConfigurations());
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}
}

/**
 * Content provider for the emongo configurator {@link TreeViewer}.
 * @author sdoerl
 * @since 12.03.2015
 */
class TreeViewContentProvider implements ITreeContentProvider {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getElements(java.lang.Object)
	 */
	@Override
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof List) {
			return ((Collection<?>)inputElement).toArray();
		}
		return (Object[])inputElement;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
	 */
	@Override
	public Object[] getChildren(Object parentElement) {
		MongoInstanceConfig config = (MongoInstanceConfig) parentElement;
		return config.getDatabaseConfigs().toArray();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
	 */
	@Override
	public Object getParent(Object element) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
	 */
	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof MongoInstanceConfig) {
			return ((MongoInstanceConfig)element).getDatabaseConfigs().size() != 0;
		}
		return false;
	}
	
}